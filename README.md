# Ersatz

Ersatz is a tool used for API mocking and service virtualization

## Installation

`npm install ersatz`

## Usage

````
$> ersatz --help

  Usage
    $ ersatz <endpoint> <options>

  Options
    -p, --port          Port
    -d, --dir           Directory to store recordings
    -r, --record        Records requests
    -m, --match-headers Headers to match against
    -c, --match-cookies Cookies to match against
````

## License

MIT