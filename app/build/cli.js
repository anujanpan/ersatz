"use strict";
exports.__esModule = true;
var meow = require("meow");
var chalk = require("chalk");
var http = require("http");
var ersatz_1 = require("./ersatz");
var cli = meow("\n  Usage\n    $ ersatz <endpoint> <options>\n\n  Options\n    -p, --port          Port\n    -d, --dir           Directory to store recordings\n    -r, --record        Records requests\n    -m, --match-headers Headers to match against\n    -c, --match-cookies Cookies to match against\n", {
    alias: {
        p: 'port',
        d: 'dir',
        r: 'record',
        m: 'match-headers',
        c: 'match-cookies'
    }
});
var port = cli.flags.port || 3004;
var endpoint = cli.input[0];
var directory = cli.flags.dir || 'ersatz/recordings';
var matchHeaders = (cli.flags.matchHeaders && cli.flags.matchHeaders.split(',').map(function (s) { return s.trim(); })) || [];
var matchCookies = (cli.flags.matchCookies && cli.flags.matchCookies.split(',').map(function (s) { return s.trim(); })) || [];
var recordMode = cli.flags.record;
if (!endpoint && recordMode) {
    console.log(chalk.red('No endpoint was given to proxy to.'));
    process.exit(1);
}
var ersatz = new ersatz_1["default"]({
    endpoint: endpoint,
    directory: directory,
    matchHeaders: matchHeaders,
    recordMode: recordMode,
    matchCookies: matchCookies
});
var server = http.createServer(function (req, res) {
    var data = '';
    req.on('data', function (newData) { return data += newData; });
    req.on('end', function () {
        req.__data = data;
        ersatz.request(req, res);
    });
    console.log(req.method + " " + req.url);
}).listen(port);
console.log(chalk.green("ersatz running on http://localhost:" + port));
console.log(chalk.blue("Running in " + (recordMode ? 'record' : 'playback') + " mode..."));
