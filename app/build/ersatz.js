"use strict";
var __assign = (this && this.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};
exports.__esModule = true;
var crypto = require("crypto");
var url = require("url");
var fs = require("fs");
var mkdirp = require("mkdirp");
var axios_1 = require("axios");
var ErsatzOptions = (function () {
    function ErsatzOptions() {
    }
    return ErsatzOptions;
}());
exports.ErsatzOptions = ErsatzOptions;
function sort(obj) {
    var ret = {};
    Object.keys(obj).sort().forEach(function (key) {
        ret[key] = obj[key];
    });
    return ret;
}
function parseCookies(rc) {
    var list = {};
    rc && rc.split(';').forEach(function (cookie) {
        var parts = cookie.split('=');
        list[parts.shift().trim()] = decodeURI(parts.join('='));
    });
    return list;
}
var Ersatz = (function () {
    function Ersatz(options) {
        var _this = this;
        this.endpoint = options.endpoint;
        this.directory = options.directory;
        this.recordMode = options.recordMode;
        this.matchHeaders = options.matchHeaders;
        this.matchCookies = options.matchCookies;
        mkdirp(this.directory, function () {
            fs.readdir(_this.directory, function (err, files) {
                _this.availableRecordings = {};
                files.forEach(function (file) {
                    _this.availableRecordings[file] = null;
                });
            });
        });
    }
    Ersatz.prototype.request = function (req, res) {
        var _this = this;
        var recordingName = this.getRecordingName(req);
        if (recordingName in this.availableRecordings) {
            this.replayRecording(recordingName, res);
        }
        else {
            if (this.recordMode) {
                this.proxyRequest(req)
                    .then(function (proxiedRes) {
                    return _this.saveRecording(recordingName, req, proxiedRes, function () {
                        _this.availableRecordings[recordingName] = null;
                        return _this.replayRecording(recordingName, res);
                    });
                })["catch"](function (err) {
                    res.statusCode = 500;
                    res.write(err.message);
                    res.end();
                });
            }
            else {
                res.statusCode = 404;
                res.setHeader('x-ersatz-id', recordingName);
                res.write("Recording not found: " + recordingName);
                res.end();
            }
        }
    };
    Ersatz.prototype.proxyRequest = function (origReq) {
        var proxyUrl = url.parse(origReq.url);
        var endpointUrl = url.parse(this.endpoint);
        proxyUrl.hostname = endpointUrl.hostname;
        proxyUrl.port = endpointUrl.port;
        proxyUrl.protocol = endpointUrl.protocol;
        var headers = __assign({}, origReq.headers);
        delete headers.host;
        return axios_1["default"]({
            method: origReq.method,
            headers: headers,
            url: url.format(proxyUrl),
            data: origReq.__data,
            transformResponse: function (data) { return data; },
            validateStatus: function () { return true; }
        });
    };
    Ersatz.prototype.replayRecording = function (fileName, res) {
        fs.readFile(this.directory + "/" + fileName, function (err, contents) {
            if (err) {
                throw err;
            }
            var data = JSON.parse(contents.toString());
            res.statusCode = data.statusCode;
            Object.keys(data.headers).forEach(function (key) { return res.setHeader(key, data.headers[key]); });
            res.setHeader('x-ersatz-id', fileName);
            res.write(data.body);
            res.end();
        });
    };
    Ersatz.prototype.saveRecording = function (fileName, req, proxiedRes, cb) {
        var response = {
            statusCode: proxiedRes.status,
            headers: proxiedRes.headers,
            body: proxiedRes.data
        };
        fs.writeFile(this.directory + "/" + fileName, JSON.stringify(response), cb);
    };
    Ersatz.prototype.getHashingParameters = function (req, parts, matchedHeaders, matchedCookies) {
        return {
            httpVersion: req.httpVersion,
            httpMethod: req.method,
            pathname: parts.pathname,
            data: req.__data,
            headers: JSON.stringify(matchedHeaders),
            cookies: JSON.stringify(matchedCookies),
            query: JSON.stringify(sort(parts.query))
        };
    };
    Ersatz.prototype.getRecordingName = function (req) {
        var matchedHeaders = (this.matchHeaders || []).map(function (header) { return req.headers && req.headers[header.toLowerCase()]; }).filter(function (header) { return !!header; });
        var parsedCookies = req.headers && req.headers.cookie ? parseCookies(req.headers.cookie) : {};
        var matchedCookies = (this.matchCookies || []).map(function (cookie) { return parsedCookies[cookie]; }).filter(function (header) { return !!header; });
        var parts = url.parse(req.url, true);
        var hash = crypto
            .createHash('md5');
        var hashParams = this.getHashingParameters(req, parts, matchedHeaders, matchedCookies);
        if (process.env.DEBUG === 'true') {
            console.log(Object.keys(hashParams).map(function (key) {
                return key + ": " + hashParams[key];
            }).join("\n"));
        }
        Object.keys(hashParams)
            .forEach(function (key) {
            hash = hash.update(hashParams[key]);
        });
        return hash.digest('hex');
    };
    return Ersatz;
}());
exports["default"] = Ersatz;
