import * as meow from 'meow';
import * as chalk from 'chalk';
import * as http from 'http';
import * as querystring from 'querystring';
import Ersatz from './ersatz';

const cli = meow(`
  Usage
    $ ersatz <endpoint> <options>

  Options
    -p, --port          Port
    -d, --dir           Directory to store recordings
    -r, --record        Records requests
    -m, --match-headers Headers to match against
    -c, --match-cookies Cookies to match against
`, {
  alias: {
    p: 'port',
    d: 'dir',
    r: 'record',
    m: 'match-headers',
    c: 'match-cookies'
  }
});

const port = cli.flags.port || 3004;
const endpoint = cli.input[0];
const directory = cli.flags.dir || 'ersatz/recordings';
const matchHeaders = (cli.flags.matchHeaders && cli.flags.matchHeaders.split(',').map((s) => s.trim())) || [];
const matchCookies = (cli.flags.matchCookies && cli.flags.matchCookies.split(',').map((s) => s.trim())) || [];
const recordMode = cli.flags.record;

if (!endpoint && recordMode) {
  console.log(chalk.red('No endpoint was given to proxy to.'));
  process.exit(1);
}

const ersatz = new Ersatz({
  endpoint,
  directory,
  matchHeaders,
  recordMode,
  matchCookies
});

const server = http.createServer((req : any, res) => {
  let data = '';
  req.on('data', (newData) => data += newData);
  req.on('end', () => {
    req.__data = data;
    ersatz.request(req, res);
  });

  console.log(`${req.method} ${req.url}`);
}).listen(port);

console.log(chalk.green(`ersatz running on http://localhost:${port}`));
console.log(chalk.blue(`Running in ${recordMode ? 'record' : 'playback'} mode...`));
