import * as crypto from 'crypto';
import * as url from 'url';
import * as fs from 'fs';
import * as mkdirp from 'mkdirp';
import axios from 'axios';

export class ErsatzOptions {
  recordMode: boolean;
  directory?: string;
  endpoint: string;
  matchHeaders: Array<string>;
  matchCookies: Array<string>;
}

function sort(obj) {
  var ret = {};

  Object.keys(obj).sort().forEach(function (key) {
    ret[key] = obj[key];
  });

  return ret;
}

function parseCookies (rc) {
  const list = {};

  rc && rc.split(';').forEach((cookie) => {
    const parts = cookie.split('=');
    list[parts.shift().trim()] = decodeURI(parts.join('='));
  });

  return list;
}

export default class Ersatz {
  recordMode: boolean;
  directory?: string;
  endpoint: string;
  matchHeaders: Array<string>;
  availableRecordings: Object;
  matchCookies: Array<string>;

  constructor (options: ErsatzOptions) {
    this.endpoint = options.endpoint;
    this.directory = options.directory;
    this.recordMode = options.recordMode;
    this.matchHeaders = options.matchHeaders;
    this.matchCookies = options.matchCookies;

    mkdirp(this.directory, () => {
      fs.readdir(this.directory, (err, files) => {
        this.availableRecordings = {};
        files.forEach((file) => {
          this.availableRecordings[file] = null;
        })
      });
    });
  }

  request(req, res) {
    const recordingName = this.getRecordingName(req);

    if (recordingName in this.availableRecordings) {
      this.replayRecording(recordingName, res);
    } else {
      if (this.recordMode) {
        this.proxyRequest(req)
          .then((proxiedRes) =>
            this.saveRecording(recordingName, req, proxiedRes,
              () => {
                this.availableRecordings[recordingName] = null;
                return this.replayRecording(recordingName, res)
              }))
          .catch((err) => {
            res.statusCode = 500;
            res.write(err.message);
            res.end();
          });
        
      } else {
        res.statusCode = 404;
        res.setHeader('x-ersatz-id', recordingName);
        res.write(`Recording not found: ${recordingName}`);
        res.end();
      }
    }
  }

  proxyRequest(origReq) {
    const proxyUrl = url.parse(origReq.url);
    const endpointUrl = url.parse(this.endpoint);
    proxyUrl.hostname = endpointUrl.hostname;
    proxyUrl.port = endpointUrl.port;
    proxyUrl.protocol = endpointUrl.protocol;
    const headers = { ...origReq.headers };
    delete headers.host;

    return axios({
      method: origReq.method,
      headers,
      url: url.format(proxyUrl),
      data: origReq.__data,
      transformResponse: data => data,
      validateStatus: () => true
    });
  }

  replayRecording(fileName, res) {
    fs.readFile(`${this.directory}/${fileName}`, (err, contents) => {
      if (err) {
        throw err;
      }
      const data = JSON.parse(contents.toString());
      res.statusCode = data.statusCode;
      Object.keys(data.headers).forEach((key) => res.setHeader(key, data.headers[key]));
      res.setHeader('x-ersatz-id', fileName);

      res.write(data.body);
      res.end();
    });
  }

  saveRecording(fileName, req, proxiedRes, cb) {
    const response = {
      statusCode: proxiedRes.status,
      headers: proxiedRes.headers,
      body: proxiedRes.data
    };

    fs.writeFile(`${this.directory}/${fileName}`, JSON.stringify(response), cb);
  }

  getHashingParameters(req, parts, matchedHeaders, matchedCookies) {
    return {
      httpVersion: req.httpVersion,
      httpMethod: req.method,
      pathname: parts.pathname,
      data: req.__data,
      headers: JSON.stringify(matchedHeaders),
      cookies: JSON.stringify(matchedCookies),
      query: JSON.stringify(sort(parts.query))
    };
  }

  getRecordingName(req) {
    const matchedHeaders = (this.matchHeaders || []).map(
      (header) => req.headers && req.headers[header.toLowerCase()]
    ).filter((header) => !!header);

    const parsedCookies = req.headers && req.headers.cookie ? parseCookies(req.headers.cookie) : {};

    const matchedCookies = (this.matchCookies || []).map(
      (cookie) => parsedCookies[cookie]
    ).filter((header) => !!header);

    const parts = url.parse(req.url, true);

    let hash = crypto
      .createHash('md5');
    const hashParams = this.getHashingParameters(req, parts, matchedHeaders, matchedCookies);

    if (process.env.DEBUG === 'true') {
      console.log(
        Object.keys(hashParams).map(key => {
          return `${key}: ${hashParams[key]}`;
        }).join("\n")
      );
    }

    Object.keys(hashParams)
      .forEach((key) => {
        hash = hash.update(hashParams[key]);
      });
      

    return hash.digest('hex');
  }
}